INTRODUCTION
------------

Limits the number of displayed value of a field in front.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/multivalue_field_restriction

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/multivalue_field_restriction


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.
